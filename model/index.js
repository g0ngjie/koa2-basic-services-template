"use strict";

const MemberModel = require("./Member");
const ErrorLogModel = require("./ErrorLog");

module.exports = {
  MemberModel,
  ErrorLogModel,
};
