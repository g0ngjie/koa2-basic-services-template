"use strict";

const Router = require("koa-router");
const router = new Router();
const { dingController } = require("./../../controller");

/* 签名 */
router.get("/getJsConfig", dingController.getJsConfig);
/* 免登 */
router.get("/login", dingController.login);

module.exports = router;
